#include "OccupancyMap.h"
OccupancyMap::OccupancyMap(QQuickItem *parent) :
    QQuickItem(parent),
    m_color(Qt::red),
    m_needUpdate(true)
{
    setFlag(QQuickItem::ItemHasContents);
    depthLevel = 16;
    _mapa = OctoInvencoRos("gotree001400.bt", depthLevel,true, &gridLocked);
    _mapa.openFile();
    highX=99;
    lowX=101;
    highY=99;
    lowY=101;
    sub =n.subscribe(_mapa.nTopic, 1, &OccupancyMap::chatterCallback, this);
}
OccupancyMap::~OccupancyMap()
{
    if(texture)
    {
        delete texture;
        texture = nullptr;
    }
}
void OccupancyMap::chatterCallback(const octomap_msgs::Octomap& msg)
{
    this->update();
}
QSGNode *OccupancyMap::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *updatePaintNodeData)
{
    std::lock_guard<std::mutex> guard(gridLocked);

//if(_mapa.fC.size()>lastSize/2&&_mapa.fC.size()>0)
//    {
    diag= cv::Mat::ones(200, 200, CV_8UC1);
    for (unsigned int i=0;i<_mapa.fC.size();++i)
    {
if(_mapa.fC[i].z()>=-0.5f&&_mapa.fC[i].z()<=0.5f){

        diag.at<uchar>(int(_mapa.fC[i].y()*5+99.5F),int(_mapa.fC[i].x()*5+99.5F)) = 130;
//        if(highY<double(_mapa.fC[i].y()*5+99.5F)){
//            highY=double(_mapa.fC[i].y()*5+99.5F);
//        }
//        if(lowY>double(_mapa.fC[i].y()*5+99.5F)){
//            lowY=double(_mapa.fC[i].y()*5+99.5F);
//        }
//        if(highX<double(_mapa.fC[i].x()*5+99.5F)){
//            highX=double(_mapa.fC[i].x()*5+99.5F);
//        }
//        if(lowX>double(_mapa.fC[i].x()*5+99.5F)){
//            lowX=double(_mapa.fC[i].x()*5+99.5F);
//        }
}
    }
    for (unsigned int i=0;i<_mapa.pC.size();++i)
    {
        if(_mapa.pC[i].z()>=-0.5f&&_mapa.pC[i].z()<=0.5f){
        //            if((_mapa.pC[i].z()<0.1&&_mapa.pC[i].z()>-0.1)&&((_mapa.pC[i].y()*4+pose.y())>bottomBorder&&(_mapa.pC[i].y()*4+pose.y())<topBorder)&&((_mapa.pC[i].x()*4+pose.x())>leftBorder&&(_mapa.pC[i].x()*4+pose.x())<rightBorder))
        diag.at<uchar>(int(_mapa.pC[i].y()*5+99.5f),int(_mapa.pC[i].x()*5+99.5f)) = 250;
//        if(highY<int(_mapa.pC[i].y()*5+99.5F)){
//            highY=int(_mapa.pC[i].y()*5+99.5F);
//        }
//        else if(lowY>int(_mapa.pC[i].y()*5+99.5F)){
//            lowY=int(_mapa.pC[i].y()*5+99.5F);
//        }
//        if(highX<int(_mapa.pC[i].x()*5+99.5F)){
//            highX=int(_mapa.pC[i].x()*5+99.5F);
//        }
//        else if(lowX>int(_mapa.pC[i].x()*5+99.5F)){
//            lowX=int(_mapa.pC[i].x()*5+99.5F);
//        }
}
    //lastSize=_mapa.fC.size();
}
    if((highX>lowX)&&(highY>lowY)){
    r = cv::Rect2d(lowX-2,lowY-1,highX-lowX+3,highY-lowY+3) ;
    //cv::Rect2d r = cv::Rect2d(0, 0, 512*sSize, 288*sSize) ;
    diag=diag(r);
    }
    //    QImage qt_img((uchar*)diag.data, diag.cols, diag.rows, diag.step, QImage::Format_RGB888);
    m_pixmap=QPixmap::fromImage(QImage (static_cast<uchar*>(diag.data), diag.cols, diag.rows, diag.step, QImage::Format_Indexed8));
    //    QRect rect(0, 0, m_pixmap.width()*sSize, m_pixmap.height()*sSize);
    //     m_pixmap = m_pixmap.copy(rect);
    //    m_pixmap.scaled(m_pixmap.width()*sSize,m_pixmap.height()*sSize);
    QSGSimpleTextureNode *node = static_cast<QSGSimpleTextureNode *>(oldNode);
    if (!node) {
        node = new QSGSimpleTextureNode();
    }
    if(texture)
    {
        delete texture;
        texture = nullptr;
    }
    texture = window()->createTextureFromImage(m_pixmap.toImage());
    node->setTexture(texture);
    node->setRect(boundingRect());

    return node;
}
void OccupancyMap::sizeReal(const float &angle, const float &pX, const float &pY)
{
    if(angle>0){
        if(sSize>0.6f)sSize=sSize*0.95f;
    }
    else{
        if(sSize<0.95f) sSize=sSize*1.05f;
    }
    pose.setZ(512*pX*sSize+minX);
    pose.setW(288*pY*sSize+minY);
}
void OccupancyMap::newPose(int &X, int &Y)
{
    pose=pose+QVector4D(X,Y,0,0);
}
void OccupancyMap::ifDisplay(bool &ifContinue)
{
    contin= ifContinue;
    if (contin==false)
    {   _mapa.sub.shutdown();
    }
    else
    {
        _mapa.openFile();
    }
}
