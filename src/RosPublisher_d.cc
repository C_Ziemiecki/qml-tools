#include "RosPublisher_d.h"

QQuickDepth::QQuickDepth(QQuickItem *parent) :
    QQuickItem(parent),
    m_color(Qt::red),
    m_needUpdate(true)
{
    sub =n.subscribe(nTopic, 1, &QQuickDepth::chatterCallback, this);
    setFlag(QQuickItem::ItemHasContents);
}
QQuickDepth::~QQuickDepth()
{
    if(texture){
      delete  texture;
        texture = nullptr;
    }
}

void QQuickDepth::chatterCallback(const sensor_msgs::ImageConstPtr& msg)
{
    auto image16 = cv_bridge::toCvCopy(msg, "16UC1");
    image16->image.convertTo(image8, CV_8UC1);
    m_pixmap=QPixmap::fromImage(QImage ((uchar*)image8.data, image8.cols, image8.rows, QImage::Format_Indexed8));
    QRect rect(image8.cols/2-image8.cols*nums/2,image8.rows/2-image8.rows*nums/2,image8.cols*nums, image8.rows*nums);
    m_pixmap = m_pixmap.copy(rect);
    m_pixmap=m_pixmap.scaled(image8.cols, image8.rows, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    this->update();
}

QSGNode *QQuickDepth::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *updatePaintNodeData)
{
    QSGSimpleTextureNode *node = static_cast<QSGSimpleTextureNode *>(oldNode);
    if (!node) {
        node = new QSGSimpleTextureNode();
    }
    if(texture)
    {
    delete texture;
        texture = nullptr;
    }
    texture = window()->createTextureFromImage(m_pixmap.toImage());
    node->setTexture(texture);
    node->setRect(boundingRect());
    return node;

}

void QQuickDepth::sizeReal(float valuef){
    nums=valuef/100;
}


