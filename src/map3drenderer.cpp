/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "map3drenderer.h"

LogoInFboRenderer::LogoInFboRenderer()
{
    sub = n.subscribe(logo._mapa.nTopic,1,&LogoInFboRenderer::checkData,this);
    logo.initialize();
    contin=true;
}
LogoInFboRenderer::~LogoInFboRenderer()
{

}
void LogoInFboRenderer::render() {
    logo.render();
}
void LogoInFboRenderer::checkData(const octomap_msgs::Octomap& msg)
{
    update();
}
void LogoInFboRenderer::setMapScale(const float &scS){
    if (scS>0){
        logo.mapScale(true);
    }
    else {
        logo.mapScale(false);
    }
    update();
}
void LogoInFboRenderer::depthLevel(const int &depth){
    logo.mapDepth(depth);
}
void LogoInFboRenderer::setPosition(const float &X, const float &Y){
    logo.mapMove(X,Y,0.0);
}
void LogoInFboRenderer::setAlfa(const float &X){
    logo.mapRotate(X,0.0,0.0);
    update();
}
void LogoInFboRenderer::setBeta(const float &X){
    logo.mapRotate(0.0,X,0.0);
    update();
}
void LogoInFboRenderer::setGamma(const float &X){
    logo.mapRotate(0.0,0.0,X);
    update();
}
void LogoInFboRenderer::mapReset(){
    logo.mapReset();
}
void LogoInFboRenderer::mapIsometry(){
    logo.mapIsometry();
}
void LogoInFboRenderer::mapX(){
    logo.mapX();
}
void LogoInFboRenderer::mapY(){
    logo.mapY();
}
void LogoInFboRenderer::mapZ(){
    logo.mapZ();
}
void LogoInFboRenderer::setDisplay(const bool &X)
{
    contin= X;
    if (contin==false)
    {   logo._mapa.sub.shutdown();
    }
    else
    {
        logo._mapa.openFile();
    }
}
QQuickFramebufferObject::Renderer *map3drenderer::createRenderer() const
{
    thisOne = new LogoInFboRenderer();
    return thisOne;
}
void map3drenderer::lsetMapScale(const float &scale)
{
    thisOne->setMapScale(scale);
}
void map3drenderer::lsetPosition(const float &X, const float &Y)
{
    thisOne->setPosition(X,Y);
}
void map3drenderer::lsetAlfa(const float &X)
{
    thisOne->setAlfa(X);
}
void map3drenderer::lsetBeta(const float &X)
{
    thisOne->setBeta(X);
}
void map3drenderer::lsetGamma(const float &X)
{
    thisOne->setGamma(X);
}
void map3drenderer::ldepthLevel(const int &X)
{
    thisOne->depthLevel(X);
}
void map3drenderer::lsetDisplay(const bool &X){
    thisOne->setDisplay(X);
}
void map3drenderer::lmapReset(){
    thisOne->mapReset();
}
void map3drenderer::lmapIsometry(){
    thisOne->mapIsometry();
}
void map3drenderer::lmapX(){
    thisOne->mapX();
}
void map3drenderer::lmapY(){
    thisOne->mapY();
}
void map3drenderer::lmapZ(){
    thisOne->mapZ();
}
