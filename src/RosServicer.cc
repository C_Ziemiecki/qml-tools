#include "RosServicer.h"
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/core/core.hpp>

QQuickService::QQuickService(QQuickItem *parent) :
    QQuickItem(parent),
    m_color(Qt::red),
    m_needUpdate(true)
{
    setFlag(QQuickItem::ItemHasContents);
}
QQuickService::~QQuickService()
{
    if(texture)
    {
        delete texture;
        texture = nullptr;
    }
}
QSGNode *QQuickService::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *)
{
    QSGSimpleTextureNode *node = static_cast<QSGSimpleTextureNode *>(oldNode);
    if (!node) {
        node = new QSGSimpleTextureNode();
    }
    if(texture)
    {
        delete texture;
        texture = nullptr;
    }
    texture = window()->createTextureFromImage(m_pixmap.toImage());
    node->setTexture(texture);
    node->setRect(boundingRect());
    return node;
}




void QQuickService::landRequest()
    {

        srv.request.check=false;
        srv.request.uav_tl=2;
        srv.request.uav_cmd = 1;
        srv.request.offb_mission = 1;
    }
void QQuickService::takeOffRequest()
    {
        srv.request.check=false;
        srv.request.uav_tl=0;
        srv.request.uav_cmd = 1;
        srv.request.offb_mission = 1;
    }
void QQuickService::executeRequest()
    {
        srv.request.check=false;
        srv.request.uav_tl=1;
        srv.request.uav_cmd = 1;
        srv.request.offb_mission = 0;
    }
void QQuickService::abortRequest()
    {
        srv.request.check=false;
        srv.request.uav_tl=1;
        srv.request.uav_cmd = 1;
        srv.request.offb_mission = 2;
    }
void QQuickService::armRequest()
    {
        srv.request.check=false;
        srv.request.uav_tl=1;
        srv.request.uav_cmd = 0;
        srv.request.offb_mission = 1;
    }
void QQuickService::disArmRequest()
    {
        srv.request.check=false;
        srv.request.uav_tl=1;
        srv.request.uav_cmd = 2;
        srv.request.offb_mission = 0;
    }


