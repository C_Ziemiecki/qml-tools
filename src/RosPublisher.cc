#include "RosPublisher.h"
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/core/core.hpp>

QQuickVideo::QQuickVideo(QQuickItem *parent) :
    QQuickItem(parent),
    m_color(Qt::red),
    m_needUpdate(true)
{
    sub =n.subscribe(nTopic, 1, &QQuickVideo::chatterCallback, this);
    setFlag(QQuickItem::ItemHasContents);
}
QQuickVideo::~QQuickVideo()
{
    if(texture)
    {
        delete texture;
        texture = nullptr;
    }
}

void QQuickVideo::chatterCallback(const sensor_msgs::ImageConstPtr& msg)
{
    m_pixmap=QPixmap::fromImage(QImage (msg->data.data(), msg->width, msg->height, QImage::Format_Indexed8));
    this->update();
}
QSGNode *QQuickVideo::updatePaintNode(QSGNode *oldNode, QQuickItem::UpdatePaintNodeData *)
{
    QSGSimpleTextureNode *node = static_cast<QSGSimpleTextureNode *>(oldNode);
    if (!node) {
        node = new QSGSimpleTextureNode();
    }
    if(texture)
    {
        delete texture;
        texture = nullptr;
    }
    texture = window()->createTextureFromImage(m_pixmap.toImage());
    node->setTexture(texture);
    node->setRect(boundingRect());
    return node;
}

void QQuickVideo::sizeReal(float valuef){
    nums=valuef/100;
}


