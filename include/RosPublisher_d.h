#ifndef ROSPUBLISHER_D_H
#define ROSPUBLISHER_D_H
#include <QObject>
#include <QString>
#include <QQuickPaintedItem>
#include <QSGGeometryNode>
#include <QQuickWindow>
#include <qsgsimpletexturenode.h>
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <cv_bridge/cv_bridge.h>
#include <QImage>
#include <QPainter>
class QQuickDepth : public QQuickItem
{
    Q_OBJECT
    QColor m_color;
    bool m_needUpdate;
public:
    QQuickDepth(QQuickItem *parent = Q_NULLPTR);
    ~QQuickDepth();
    void chatterCallback(const sensor_msgs::ImageConstPtr& msg);
    Q_INVOKABLE  void sizeReal(float valuef);
    float nums=1;
    QPixmap m_pixmap;
    ros::NodeHandle n;
    ros::Subscriber sub;
    cv::Mat image8;
    QSGTexture *texture = nullptr;
    std::string nTopic ="/camera/depth/image_raw";
    int img_flag =0;

protected:
    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *updatePaintNodeData);
};
#endif // ROSPUBLISHER_D_H
