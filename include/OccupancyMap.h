#ifndef OccupancyMap_H
#define OccupancyMap_H
#include <QObject>
#include <QQuickPaintedItem>
#include <QSGGeometryNode>
#include <QQuickWindow>
#include <qsgsimpletexturenode.h>
#include <QSGFlatColorMaterial>
#include <sstream>
//#include <gst/gst.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <QImage>
#include <QPainter>
#include <QGraphicsView>
#include <octoInvRos.h>
#include <PositionRos.h>

class OccupancyMap : public QQuickItem
{
    Q_OBJECT
    QColor m_color;
    bool m_needUpdate;
public:
    OccupancyMap(QQuickItem *parent = Q_NULLPTR);
    ~OccupancyMap();
    Q_INVOKABLE void newPose(int &X, int &Y);
    Q_INVOKABLE void sizeReal(const float &angle, const float &pX, const float &pY);
    Q_INVOKABLE void ifDisplay(bool &ifContinue);
    void chatterCallback(const octomap_msgs::Octomap &msg);
    std::mutex gridLocked;
    ros::NodeHandle n;
    ros::Subscriber sub;
    OctoInvencoRos _mapa;
    QSGTexture *texture = nullptr;
    int depthLevel;
    cv::Mat diag;
    QVector4D pose=QVector4D(0,0,144,256);
   float mScale=1;
    float sSize=1;
    float minX=0;
    float minY=0;
    qreal highX=0;
    qreal lowX=200;
    qreal highY=0;
    qreal lowY=100;
    bool contin=true;
    PositionRos _positionDrone;
    QPixmap m_pixmap;
    float bottomBorder, topBorder, rightBorder, leftBorder;
    cv::Rect2d r;
protected:
    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *updatePaintNodeData);

};
#endif // OCCUPANCYMAP_H
