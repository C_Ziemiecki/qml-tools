#ifndef ROSPUBLISHER_H
#define ROSPUBLISHER_H
#include <QObject>
#include <QString>
#include <QQuickPaintedItem>
#include <QSGGeometryNode>
#include <QQuickWindow>
#include <qsgsimpletexturenode.h>
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <cv_bridge/cv_bridge.h>
#include <QImage>
//#include <QPainter>
class QQuickVideo : public QQuickItem
{
    Q_OBJECT
    QColor m_color;
    bool m_needUpdate;
public:
    QQuickVideo(QQuickItem *parent = Q_NULLPTR);
    ~QQuickVideo();
    void chatterCallback(const sensor_msgs::ImageConstPtr& msg);
    Q_INVOKABLE  void sizeReal(float valuef);
    QPixmap m_pixmap;
    ros::NodeHandle n;
    ros::Subscriber sub;
    std::string nTopic="/vi_sensor/camera/image_raw";
    cv::Mat raw_image;
    QSGTexture *texture = nullptr;
    float nums=1;
protected:
    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *);
};
#endif // ROSPUBLISHER_H
