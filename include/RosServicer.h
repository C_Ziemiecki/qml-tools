#ifndef ROSSERVICER_H
#define ROSSERVICER_H
#include <QObject>
#include <QString>
#include <QQuickPaintedItem>
#include <QSGGeometryNode>
#include <QQuickWindow>
#include <qsgsimpletexturenode.h>
#include "ros/ros.h"
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <cv_bridge/cv_bridge.h>
#include <QImage>
#include "ButtonService.h"
#include <cstdlib>
//#include <QPainter>
class QQuickService : public QQuickItem
{
    Q_OBJECT
    QColor m_color;
    bool m_needUpdate;
public:


    QQuickService(QQuickItem *parent = Q_NULLPTR);
    ~QQuickService();
    Q_INVOKABLE  void landRequest();
    Q_INVOKABLE  void takeOffRequest();
    Q_INVOKABLE  void executeRequest();
    Q_INVOKABLE  void abortRequest();
    Q_INVOKABLE  void armRequest();
    Q_INVOKABLE  void disArmRequest();

    QPixmap m_pixmap;
    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<button_service::ButtonService>("SendPX");
    button_service::ButtonService srv;
    ros::Subscriber sub;
    cv::Mat raw_image;
    QSGTexture *texture = nullptr;
    float nums=1;
protected:
    QSGNode *updatePaintNode(QSGNode *oldNode, UpdatePaintNodeData *);
};
#endif // ROSSERVICER_H
