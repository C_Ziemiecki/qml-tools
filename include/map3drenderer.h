/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef map3drenderer_H
#define map3drenderer_H

#include <QtQuick/QQuickFramebufferObject>
#include "openGLrenderer.h"
#include <QtGui/QOpenGLFramebufferObject>
#include <QtQuick/QQuickWindow>
#include <qsgsimpletexturenode.h>
class LogoInFboRenderer : public QQuickFramebufferObject::Renderer
{
public:
    LogoInFboRenderer();
    ~LogoInFboRenderer();
    void render() ;
    void checkData(const octomap_msgs::Octomap& msg);
    void setAlfa(const float &X);
    void setBeta(const float &X);
    void setGamma(const float &X);
    void mapReset();
    void mapIsometry();
    void setDisplay(const bool &X);
    void setMapScale(const float &scS);
    void depthLevel(const int &depth);
    void setPosition(const float &X, const float &Y);
    void mapX();
    void mapY();
    void mapZ();
    QOpenGLFramebufferObject *createFramebufferObject(const QSize &size)
    {
        QOpenGLFramebufferObjectFormat format;
        format.setAttachment(QOpenGLFramebufferObject::Depth);
        return new QOpenGLFramebufferObject(size, format);
    }
    openGLrenderer logo;
    bool contin;
    ros::NodeHandle n;
    ros::Subscriber sub;
};
class map3drenderer : public QQuickFramebufferObject
{
    Q_OBJECT
public:
    Renderer *createRenderer() const;
    Q_INVOKABLE void lsetMapScale(const float &scale);
    Q_INVOKABLE void lsetPosition(const float &X, const float &Y);
    Q_INVOKABLE void lsetAlfa(const float &X);
    Q_INVOKABLE void lsetBeta(const float &X);
    Q_INVOKABLE void lsetGamma(const float &X);
    Q_INVOKABLE void lmapReset();
    Q_INVOKABLE void ldepthLevel(const int &X);
    Q_INVOKABLE void lsetDisplay(const bool &X);
    Q_INVOKABLE void lmapIsometry();
    Q_INVOKABLE void lmapX();
    Q_INVOKABLE void lmapY();
    Q_INVOKABLE void lmapZ();
    mutable Renderer *thisOne;
};
#endif
