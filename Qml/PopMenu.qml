
import QtQuick                  2.3
import QtQuick.Controls         1.2
import QtQuick.Controls.Styles  1.4
import QtGraphicalEffects       1.0
import QtQuick.Layouts          1.12

import QGroundControl.Palette       1.0
import QGroundControl.ScreenTools   1.0

Rectangle
{
    id: root
    property var _lactiveVehicle:    QGroundControl.multiVehicleManager.activeVehicle

    property var _lflightModes:      _activeVehicle ? _activeVehicle.flightModes : [ ]
    property bool _lshow: false
    property bool _lhide: false
    property bool _ltimer: false
    property real _lpromile: 0

    visible: false
    //color: Qt.rgba(0.2,0.3,0.28,_lpromile/1000)
    color: Qt.rgba(0.2,0.3,0.28,_lpromile/4000)
    y: -0.3*_lpromile
    height: 0.3*_lpromile
    width: parent.width+16
    anchors.left: parent.left
    clip: true
    anchors.leftMargin: -8
    radius: 10
    z: parent.z-1
    PropertyAnimation { id: animation1;
        target: root;
        running: _lshow
        property: "_lpromile";
        to: 1000;
        duration: 1500 }
    PropertyAnimation { id: animation2;
        target: root;
        running: _lhide
        property: "_lpromile";
        to: 0;
        duration: 1500 }
    Column
    {
        id: menuColumn
        width: parent.width
        spacing: 3
        Rectangle
        {
            id: title
            height: 10
            color: "transparent"
        }

        Repeater
        {
            model: _flightModes
            Button
            {
                anchors.left: menuColumn.left
                anchors.leftMargin: 4
                anchors.right: menuColumn.right
                anchors.rightMargin: 4
                text: _flightModes[index]
                height: 44
                style: ButtonStyle {
                    background: Rectangle {

                        border.width: control.activeFocus ? 2 : 1
                        border.color: "#888"
                        radius: 4
                        gradient: Gradient {
                            GradientStop { position: 0 ; color:"transparent"}
                            GradientStop { position: 1 ; color:Qt.rgba(1.0,0.8,0.7,_lpromile/1000)}
                        }
                    }
                }
                onClicked: {
                    _lactiveVehicle.flightMode = text
                    _ltimer=true
                }
            }
        }



    }

    Timer
    {
        id: timer33
        running: _ltimer

        onTriggered:
        {

            root.visible= false
            _ltimer= false
            _lhide=false

        }
        interval: 1600
        repeat: true
    }
}
